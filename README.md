MA SSO IDP
==========
Message Agency Single Sign On Identity Provider.

*For the Service Provider (SP) version, see: https://bitbucket.org/messageagency/ma_sso_sp*

Purpose
-------
This module is meant to run some sanity-checks to help troubleshoot your setup. It's purpose is NOT to do the work of configuring your Drupal-based Simple SAML IDP, but to provide some sane defaults and help walk through your install. As such, very particular assumptions are made about how your IDP (and SPs) will be set up. Default config files are provided for reference, which you may need to customize, specifically `saml20-sp-remote.php`. See [Adding service providers](#Adding service providers).

Quick start for using this module
---------------------------------
- `composer require messageagency/ma_sso_idp`
- `drush en ma_sso_idp`
- go to `admin/reports/status` and work through any errors, per below

Assumptions / Prerequisites
--------------------------
* This Drupal site will serve as primary and default IDP for other Drupal sites
* This Drupal site will implement only one IDP
* Installation and config will be managed by composer
* `composer.json` is in your VCS root
* Your drupal webroot is `/web`
* Your simple saml web install will be symlinked from `https://example.com/simplesaml`
* This module will be installed to `web/modules/message_agency/ma_sso_idp`
* Drupal private files directory lives at `web/sites/default/files/private`
* Everything is served over `https://`, though self-signed certs are ok.

Detailed IDP setup guide
------------------------
1. **Create the simplesaml symlink**

    Add a symlink in your webroot pointing to simplesaml web directory. 
    
    For simplesaml versions 2.0+ (Drupal 10+)
    ```
        cd ./web
        ln -s ../vendor/simplesamlphp/simplesamlphp/public simplesaml
    ``` 
   For simplesaml versions 1.x
   ```
   cd ./web
   ln -s ../vendor/simplesamlphp/simplesamlphp/www simplesaml
    ``` 

    ------

2. **Composer tasks**

    Add the following rules to composer.json, so that composer tasks don't destroy your saml config. See also [Adding service providers](#Adding service providers) about ongoing management of service provider definitions. 

    ```
    "post-install-cmd": [
        "cp web/modules/message_agency/ma_sso_idp/sso/config.php ./vendor/simplesamlphp/simplesamlphp/config/config.php",
        "cp web/modules/message_agency/ma_sso_idp/sso/authsources.php ./vendor/simplesamlphp/simplesamlphp/config/authsources.php",
        "cp web/modules/message_agency/ma_sso_idp/sso/saml20-sp-remote.php ./vendor/simplesamlphp/simplesamlphp/metadata/saml20-sp-remote.php",
        "cp web/modules/message_agency/ma_sso_idp/sso/saml20-idp-hosted.php ./vendor/simplesamlphp/simplesamlphp/metadata/saml20-idp-hosted.php"
    ],
    "post-update-cmd": [
        "cp web/modules/message_agency/ma_sso_idp/sso/config.php ./vendor/simplesamlphp/simplesamlphp/config/config.php",
        "cp web/modules/message_agency/ma_sso_idp/sso/authsources.php ./vendor/simplesamlphp/simplesamlphp/config/authsources.php",
        "cp web/modules/message_agency/ma_sso_idp/sso/saml20-sp-remote.php ./vendor/simplesamlphp/simplesamlphp/metadata/saml20-sp-remote.php",
        "cp web/modules/message_agency/ma_sso_idp/sso/saml20-idp-hosted.php ./vendor/simplesamlphp/simplesamlphp/metadata/saml20-idp-hosted.php"
    ],
    ``` 
    ------

3. **Require this module or reinstall to deploy configs**
    
    To deploy the configs from previous step, require this module or run `composer install`.
    ```
        composer require messageagency/ma_sso_idp
    ```
    ------

4. **Enable Module**
   ```
       drush en ma_sso_idp
   ```

5. **Install certificates**

    ```
    openssl req -newkey rsa:3072 -new -x509 -days 3652 -nodes -out cert.pem -keyout key.pem
    mkdir -p ./web/sites/default/files/private/simplesaml/cert
    mv *.pem ./web/sites/default/files/private/simplesaml/cert
    ```
    ------

6. Check your install on your Drupal site's status report, e.g. "https://example.com/admin/reports/status"

    ------

7. Visit your simplesaml install "Authentication" tab, e.g. `https://example.com/simplesaml/module.php/core/authenticate.php`. There should be a link to `drupal-userpass`. If following this link takes you to the drupal login page, you're on the right track.

Additional considerations
-------------------------

#### .htaccess rules for apache
If you're running apache, you'll need to authorize access to simplesaml by adding the following in your `.htaccess` file.

Immediately prior to the line 
```  
RewriteRule "^(.+/.*|autoload)\.php($|/)" - [F]
```
add the following directives:
```
  # Allow access to simplesamlphp scripts.
  RewriteCond %{REQUEST_URI} !^/simplesaml
```

#### Adding service providers
The file `saml20-sp-remote.php` is provided for reference. You'll probably want to copy this file to your project's version control, modify to include your own SP definitions, and re-define the composer task for deploying this file.
