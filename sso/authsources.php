<?php

$docroot=  empty($_ENV['DOCROOT']) ? '/Users/bauman/Sites/messageweb-d8/web' : $_ENV['DOCROOT'];

$config = [
  // This is a authentication source which handles admin authentication.
  'admin' => [
    // The default is to use core:AdminPassword, but it can be replaced with
    // any authentication source.

    'core:AdminPassword',
  ],
  'drupal-userpass' => [
    'drupalauth:External',

    // The filesystem path of the Drupal directory.
    'drupalroot' => $docroot,

    // Whether to turn on debug
    'debug' => true,

    // Cookie name.
    'cookie_name' => 'SimpleSAMLdrupalauth',

    // the URL of the Drupal logout page
    'drupal_logout_url' => 'https://' . $_SERVER['HTTP_HOST'] . '/user/logout',

    // the URL of the Drupal login page
    'drupal_login_url' => 'https://' . $_SERVER['HTTP_HOST'] . '/user/login',

    // Which attributes should be retrieved from the Drupal site.
    'attributes' => [
      ['field_name' => 'uid', 'attribute_name' => 'uid'],
      ['field_name' => 'roles', 'attribute_name' => 'roles', 'field_property' => 'target_id'],
      ['field_name' => 'name', 'attribute_name' => 'username'],
      ['field_name' => 'mail', 'attribute_name' => 'mail'],
//      ['field_name' => 'field_name_first', 'attribute_name' => 'firstName'],
//      ['field_name' => 'field_name_last', 'attribute_name' => 'lastName'],
//      ['field_name' => 'field_organization', 'attribute_name' => 'organization'],
//      ['field_name' => 'field_newsletter_subscriptions', 'attribute_name' => 'newsletterSubscriptions'],
//      ['field_name' => 'field_newsletter_issue_subscript', 'attribute_name' => 'newsletterIssueSubscriptions'],
    ],
  ],
];
