<?php

/**
 * @file
 * SAML 2.0 remote SP metadata for SimpleSAMLphp.
 *
 * See: https://simplesamlphp.org/docs/stable/simplesamlphp-reference-sp-remote.
 */

$drupal_sites = [
  "message.test",
  "opigno.test",
  "dev-testopigno.pantheonsite.io",
];
foreach ($drupal_sites as $base_domain) {
  $metadata["https://$base_domain/simplesaml/module.php/saml/sp/metadata.php/default-sp"] = [
    "SingleLogoutService" =>
      [
        0 =>
          [
            "Binding" => "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Redirect",
            "Location" => "https://$base_domain/simplesaml/module.php/saml/sp/saml2-logout.php/default-sp",
          ],
        1 =>
          [
            "Binding" => "urn:oasis:names:tc:SAML:2.0:bindings:SOAP",
            "Location" => "https://$base_domain/simplesaml/module.php/saml/sp/saml2-logout.php/default-sp",
          ],
      ],
    "AssertionConsumerService" =>
      [
        0 =>
          [
            "index" => 0,
            "Binding" => "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-POST",
            "Location" => "https://$base_domain/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp",
          ],
        1 =>
          [
            "index" => 1,
            "Binding" => "urn:oasis:names:tc:SAML:1.0:profiles:browser-post",
            "Location" => "https://$base_domain/simplesaml/module.php/saml/sp/saml1-acs.php/default-sp",
          ],
        2 =>
          [
            "index" => 2,
            "Binding" => "urn:oasis:names:tc:SAML:2.0:bindings:HTTP-Artifact",
            "Location" => "https://$base_domain/simplesaml/module.php/saml/sp/saml2-acs.php/default-sp",
          ],
        3 =>
          [
            "index" => 3,
            "Binding" => "urn:oasis:names:tc:SAML:1.0:profiles:artifact-01",
            "Location" => "https://$base_domain/simplesaml/module.php/saml/sp/saml1-acs.php/default-sp/artifact",
          ],
      ],
  ];
}
