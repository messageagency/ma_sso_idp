<?php

$config = [
  'default-sp' => [
    'saml:SP',
    'idp' => 'https://ismp.test/simplesaml/saml2/idp/metadata.php',
    'privatekey' => 'new.pem',
    'certificate' => 'cert.pem',
  ],
];
