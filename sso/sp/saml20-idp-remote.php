<?php

$metadata['https://pr-156-messageweb-d8.pantheonsite.io/'] = array(
  'metadata-set' => 'saml20-idp-remote',
  'entityid' => 'https://pr-156-messageweb-d8.pantheonsite.io/',
  'SingleSignOnService'  => 'https://pr-156-messageweb-d8.pantheonsite.io/simplesaml/saml2/idp/SSOService.php',
  'SingleLogoutService'  => 'https://pr-156-messageweb-d8.pantheonsite.io/simplesaml/saml2/idp/SingleLogoutService.php',
  'certificate'          => 'cert.pem',
);
